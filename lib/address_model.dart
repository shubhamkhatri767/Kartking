class addressmodel {
  String? name;
  String? mobileNo;
  String? area;
  String? street;
  String? landMark;
  String? city;
  String? state;
  String? pinCode;
  String? addressType;

  addressmodel({
    this.addressType,
    this.area,
    this.city,
    this.landMark,
    this.mobileNo,
    this.name,
    this.pinCode,
    this.state,
    this.street,
  });
}
